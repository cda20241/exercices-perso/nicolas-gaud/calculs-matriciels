import numpy as np


def initmatrix(nx, ny):
    """
    This function creates an empty matrix filled with zeros

    :param nx: ny: two integers that represents the size of the matrix

    :return: a matrix
    """
    matrix = np.zeros((nx, ny))
    return matrix


def change_value(nx, ny, value, matrix):
    """
    This function changes value at given coordinates

    :param nx: ny: two integers that represents the coordinates

    :param value: the new value

    :param matrix: the matrix in which to change the value

    :return: a matrix
    """
    matrix[nx, ny] = value
    return matrix


def matrix_identity(size):
    """
    This function creates an identity matrix

    :param size: ths size of the matrix wanted

    :return: a matrix
    """
    matrix = np.eye(size)
    return matrix


def print_matrix(matrix):
    """
    This function shows the values inside the given matrix

    :param matrix: the matrix to show

    :return: none
    """
    rows, columns = matrix.shape
    for i in range(rows):
        for j in range(columns):
            print(matrix[i, j])


def dimension_check(matrix1, matrix2):
    """
    This function compares the size of two matrices

    :param matrix1: first matrix to be compared

    :param matrix2: second matrix to be compared

    :return: True if same size, False else
    """
    if matrix1.shape == matrix2.shape:
        return True
    else:
        return False


def sum_matrix(matrix1, matrix2):
    """
    This function add the value of the two given matrix and throws an exception MatrixDimensionError if they don't
    have the same dimensions

    :param matrix1: first matrix to add

    :param matrix2: second matrix to add

    :return: the new matrix with new values
    """
    try:
        matrixresult = matrix1 + matrix2
        return matrixresult
    except Exception as ex:
        print(ex)
        print('MatrixDimensionError : Les deux matrices n\'ont pas les mêmes dimensions')


def is_element(matrix, number):
    """
    This function checks if an element is in the given matrix and prints the coordinate(s) of this element

    :param matrix: the matrix to check in

    :param number: the element to look for

    :return: a string containing the coordinates
    """
    rows, columns = matrix.shape
    result = ''
    isfound = False
    for i in range(rows):
        for j in range(columns):
            if matrix[i, j] == number:
                result += 'On retrouve le nombre "' + str(number) + '" aux coordonnées : (' + str(i) + ',' + str(
                    j) + ')\n'
                isfound = True
    if not isfound:
        result = 'Le nombre n\'a pas été trouvé dans la matrice.'
    return result


def scalar_product(matrix, number):
    """
    This function multiplies the given matrix with a number

    :param matrix: the matrix to be multiplied

    :param number: the number to multiply the matrix with

    :return: the matrix with new values
    """
    return matrix * number


def matrix_product(matrix1, matrix2):
    """
    This function multiplies two given matrices if they have the same dimensions, prints an exception message else

    :param matrix1: first matrix to multiply

    :param matrix2: first matrix to multiply

    :return: a nex matrix with the result of the product
    """
    try:
        matrixresult = matrix1 * matrix2
        return matrixresult
    except Exception as ex:
        print(ex)
        # raise Exception('MatrixDimensionError : Les deux matrices n\'ont pas les mêmes dimensions')
        #if we want to continue the programme anyway 3 possibilities :
        # print('MatrixDimensionError : Les deux matrices n\'ont pas les mêmes dimensions')
        # or python command "pass" that does nothing except continuing the program
        # or let python show its message as above

# matrice1 = np.random.randint(low=1, high=9, size=(10, 10))
# matrice2 = np.random.randint(low=1, high=9, size=(9, 9))
# matrice3 = np.random.randint(low=1, high=9, size=(10, 10))
#
# print(matrix_product(matrice1, matrice2))
# print(matrix_product(matrice1, matrice3))
