import unittest
from main import initmatrix
from main import change_value


class TestFunctions(unittest.TestCase):
    # Nicolas GAUD

    def test_initmatrix(self):
        """
        This function ensures that len(initmatrix(3 , 3)) ==  3
        """
        self.assertEqual(len(initmatrix(3, 3)), 3)

    def test_change_value(self):
        """
        This function ensures that len(initmatrix(3 , 3)) == 3
        """
        matrix_tmp = initmatrix(3, 3)
        modified_matrix = change_value(0, 0, 9, matrix_tmp)
        self.assertEqual(modified_matrix[0][0], 9)

    def test_dimension_check(self):
        """
        This function ensures that  two matrices shape are equal
        """
        matrix1 = initmatrix(9, 9)
        matrix2 = initmatrix(9, 10)
        matrix3 = initmatrix(9, 9)
        self.assertNotEqual(matrix1.shape, matrix2.shape)
        self.assertEqual(matrix1.shape, matrix3.shape)

